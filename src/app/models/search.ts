import { iFilm } from "./film";

 
export interface iSearchResults {
    page: number;
    total_results: number;
    total_pages: number;
    results?: (iFilm)[] | null;
} 