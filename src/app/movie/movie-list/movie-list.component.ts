import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config.service';
import { FilmsService } from 'src/app/services/films.service';
import { FormGroup, FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { iSearchResults } from 'src/app/models/search';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
	selector: 'app-movie-list',
	templateUrl: './movie-list.component.html',
	styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
	public form: FormGroup;
	public searchResults: iSearchResults;
	public currentPage: number = 1;
	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		public filmService: FilmsService
	) {
		this.form = new FormGroup({
			queryString: new FormControl('')
		});
		this.form.valueChanges.pipe(debounceTime(700)).subscribe(value => {
			this.changeQueryString(value.queryString);
			this.router.navigate(['.'], {
				queryParams: {
					queryString: value.queryString
				}
			})
		});
		this.activatedRoute.queryParams.subscribe(queryParams => {
			if (queryParams.queryString) {
				this.form.controls.queryString.setValue(queryParams.queryString)
			}
		});
	}

	ngOnInit() {


	}

	public changeQueryString(queryString: string) {
		if (!queryString) { return false }
		this.currentPage = 1;
		this.filmService.searchFilm(queryString).subscribe(res => {
			this.searchResults = res;
		});
	}

	public loadNextPage() {

		if (this.currentPage < this.searchResults.total_pages && this.searchResults.total_pages > 1) {
			this.currentPage++;
			this.filmService.searchFilm(this.form.controls.queryString.value, this.currentPage).subscribe(res => {
				this.searchResults = res;
			});
		}
	}
	public loadPrevPage() {
		if (this.currentPage >= 2) {
			this.currentPage--;
			this.filmService.searchFilm(this.form.controls.queryString.value, this.currentPage).subscribe(res => {
				this.searchResults = res;
			});
		}
	}

}
