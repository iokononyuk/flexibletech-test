import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FilmsService } from 'src/app/services/films.service';
import { iSingleFilm } from 'src/app/models/film';

@Component({
  selector: 'app-movie-page',
  templateUrl: './movie-page.component.html',
  styleUrls: ['./movie-page.component.css']
})
export class MoviePageComponent implements OnInit {
  public film: iSingleFilm;
  constructor(
    private activatedRoute: ActivatedRoute,
    public filmsService: FilmsService
  ) {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.loadFilmByID(params.id);
      }
    })
  }

  ngOnInit() {
  }

  public loadFilmByID(id: number) {
    this.filmsService.getFilmByID(id).subscribe(res => {
      this.film = res;
    })
  }

}
