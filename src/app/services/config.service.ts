import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  public API_URL: string = 'https://api.themoviedb.org/3/search/movie';
  private API_KEY: string = '75f8003b3dbfe594bb54ecd2fe13b6c8';
  constructor() { }

  public getApiUrl(): string {
    return `${this.API_URL}?api_key=${this.API_KEY}`;
  }
  public getApiKey() {
    return this.API_KEY;
  }
}
