import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { iSearchResults } from '../models/search';
import { iSingleFilm } from '../models/film';


@Injectable({
  providedIn: 'root'
})
export class FilmsService {
  public favoritesFilms: number[] = [];
  constructor(
    public config: ConfigService,
    private http: HttpClient,
  ) {
  }

  public searchFilm(queryString: string, page?: number): Observable<iSearchResults> {
    return this.http.get<iSearchResults>(`${this.config.getApiUrl()}&query=${queryString}${page ? '&page=' + page : ''}`);
  }

  public getFilmByID(id: number): Observable<iSingleFilm> {
    return this.http.get<iSingleFilm>(`https://api.themoviedb.org/3/movie/${id}?api_key=${this.config.getApiKey()}`)
  }

}
