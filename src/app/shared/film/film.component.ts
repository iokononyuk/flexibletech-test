import { Component, OnInit, Input } from '@angular/core';
import { iFilm } from 'src/app/models/film';
import { FilmsService } from 'src/app/services/films.service';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit {
  @Input() film: iFilm;
  constructor(
    public filmsService: FilmsService
  ) { }

  ngOnInit() {
    
  }
 
}
