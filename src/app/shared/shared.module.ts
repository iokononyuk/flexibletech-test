import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuiModule } from 'ng2-semantic-ui';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FilmComponent } from './film/film.component'; 
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    SuiModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule
  ],
  exports: [
    SuiModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FilmComponent
  ],
  declarations: [FilmComponent]
})
export class SharedModule { }
